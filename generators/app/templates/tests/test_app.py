from unittest import TestCase

from <%= package %> import app


class TestApp(TestCase):
    def test_success(self) -> None:
        app.main()
