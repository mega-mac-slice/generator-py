#!/usr/bin/env python<%= pythonVersion %>
from setuptools import setup

setup(
    name="<%= name %>",
    version="0.0.0",
    packages=["<%= package %>"],
    url="https://gitlab.com/mega-mac-slice/<%= package %>",
    license="MIT",
    author="mega-mac-slice",
    author_email="megamacslice@protonmail.com",
    description="<%= description %>",
    entry_points={"console_scripts": ["<%= package %> = <%= package %>.app:main"]},
    data_files=["README.md", "requirements.txt"],
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    install_requires=open("requirements.txt").readlines(),
    tests_require=["mock"],
    test_suite="tests",
    extras_require={
        "dev": [
            "black",
            "bumpversion",
            "isort",
            "mock",
            "mypy",
            "pycodestyle",
            "tox",
            "twine",
        ]
    },
)
