"use strict";
const Generator = require("yeoman-generator");
const chalk = require("chalk");
const yosay = require("yosay");
const path = require("path");

module.exports = class extends Generator {
  prompting() {
    // Have Yeoman greet the user.
    this.log(
      yosay(`Welcome to the epic ${chalk.red("generator-py")} generator!`)
    );

    const dirName = process.cwd().split(path.sep).pop();
    const prompts = [
      {
        type: 'input',
        name: 'name',
        message: 'Application Name',
        default: dirName.toLowerCase()
      },
      {
        type: 'input',
        name: 'package',
        message: 'Application Package',
        default: dirName.toLowerCase()
      },
      {
        type: 'input',
        name: 'description',
        message: 'Description',
        default: ''
      },
      {
        type: 'input',
        name: 'pythonVersion',
        message: 'Python Version',
        default: '3.6'
      },
      {
        type: 'input',
        name: 'port',
        message: 'Application Port',
        default: 8080
      }
    ];

    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props;
    });
  }
  addTemplates() {
    return this.fs.copyTpl(
      this.templatePath(),
      this.destinationPath(),
      {...this.props}
    );
  }

  addDotFileTemplates() {
    return this.fs.copyTpl(
      this.templatePath('.*'),
      this.destinationPath(),
      {...this.props}
    );
  }

  gitDependencies() {
    this.spawnCommandSync('git', ['init']);
    this.spawnCommandSync('git', ['add', '.']);
    this.spawnCommandSync('git', ['commit', '-m', 'init']);
  }


  writing() {
  }

  install() {
  }
};
